import React from 'react';
import './Card.css';

const Card = ({user}) => {
  return (
    <div className="card">
      <div className="card-info">
        <div className="card-box">
          <h3>Name: {user?.name}</h3>
          <div>Phone: {user?.phone}</div>
          <div>Email: {user?.email}</div>
          <div>Username: {user?.username}</div>
          <div>Website: <a href={'http://' + user?.website} target="_blank">{user?.website}</a></div>

          <h4 className="company-title">Company</h4>
          <div>Name: {user?.company?.name}</div>
          <div>BS: {user?.company?.bs}</div>
          <div>Catch phrase: {user?.company?.catchPhrase}</div>
        </div>

        <div className="address-block card-box">
          <h4>Address</h4>
          <ul>
            <li>Country: {user?.address.country}</li>
            <li>State: {user?.address.state}</li>
            <li>City: {user?.address.city}</li>
            <li>StreetA: {user?.address.streetA}</li>
            <li>StreetB: {user?.address.streetB}</li>
            <li>StreetC: {user?.address.streetC}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Card;
