import axios from "axios";
import {apiURL} from "./apiURL";

const requester = axios.create({
  baseURL: apiURL
});

export default requester;
