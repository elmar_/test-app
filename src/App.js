import React, {useEffect, useState} from 'react';
import requester from "./requester";
import Card from "./components/Card/Card";
import './App.css';
import {getLocalStorage, setLocalStorage} from "./localStorage";

const App = () => {
  const [users, setUsers] = useState([]);
  const [form, setForm] = useState({
    search: '',
    select: 'name'
  });

  useEffect(() => {
    // fetching users from server
    const fetchData = async () => {
      try {
        const users = getLocalStorage('users');
        if (users && users.length > 0) {
          setUsers(users);
        } else {
          const res = await requester.get('/users');
          setUsers(res.data);
          setLocalStorage('users', res.data);
        }
      } catch (e) {
        console.error('error', e);
        alert('error, try again');
      }
    }

    fetchData();
  }, []);

  const onSubmit = e => {
    e.preventDefault();
  };

  return (
    <div>
      <header className="header">
        <div className="container">
          <h1>Contact application</h1>
        </div>
      </header>

      <main className="container">
        <div className="form-block">
          <form onSubmit={onSubmit} className="search-form">
            <div className="select-block">
              <label htmlFor="select">Search by</label>
              <select
                id="select"
                value={form.select}
                name="select"
                onChange={e => setForm({
                  ...form,
                  [e.target.name]: e.target.value
                })}
              >
                <option value="name">Name</option>
                <option value="phone">Phone</option>
                <option value="website">Website</option>
                <option value="email">Email</option>
                <option value="username">Username</option>
              </select>
            </div>

            <div className="input">
              <input
                type="text"
                placeholder={"Search by " + form.select}
                id="search"
                name="search"
                onChange={e => setForm({
                  ...form,
                  [e.target.name]: e.target.value
                })}
              />
              <button type="submit" className="btn">search</button>
            </div>
          </form>
        </div>

        <div className="content">
          {
            // filter and mapping users
            users.length > 0
              ? users.filter(user => user[form.select].toLowerCase().includes(form.search.toLowerCase()))
                .map(user => <Card user={user} key={user.id} />)
              : <h3 className="empty-title">No users</h3>
          }
        </div>
      </main>

    </div>
  );
};

export default App;
